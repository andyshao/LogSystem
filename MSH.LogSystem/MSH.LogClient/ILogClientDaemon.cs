﻿using log4net.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MSH.LogClient
{
    /// <summary>
    /// Appender守护者
    /// </summary>
    internal interface ILogClientDaemon
    {
        /// <summary>
        /// 开启守护线程和读取线程
        /// </summary>
        void Start();

        /// <summary>
        /// 日志消息进入队列
        /// </summary>
        void ToQueue(LiteDbData<LogRequest> logRequest);
    }
}
